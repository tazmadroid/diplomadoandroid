package com.cybertech.contacts;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnDateListener{

  private TextInputLayout nameTextInputLayout=null;
  private TextInputLayout lastnameTextInputLayout=null;
  private TextInputLayout ageTextInputLayout=null;
  private TextInputLayout countryTextInputLayout=null;
  private ImageButton birthdayImageButton=null;
  private RadioGroup sexRadioGroup=null;
  private Spinner statesSpinner=null;
  private CheckBox spanishCheckBox=null;
  private EditText addressEditText=null;

  private List<String> intereses=new ArrayList<>();

  private char sex='M';
  private String state=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    nameTextInputLayout=(TextInputLayout)findViewById(R.id.nameTextInputLayout);
    birthdayImageButton =(ImageButton) findViewById(R.id.birthdayImageButton);
    lastnameTextInputLayout = (TextInputLayout) findViewById(R.id.lastnameTextInputLayout);
    ageTextInputLayout = (TextInputLayout) findViewById(R.id.ageTextInputLayout);
    countryTextInputLayout=(TextInputLayout) findViewById(R.id.countryTextInputLayout);
    sexRadioGroup = (RadioGroup) findViewById(R.id.sexRadioGroup);
    statesSpinner = (Spinner) findViewById(R.id.statesSpinner);
    spanishCheckBox = (CheckBox) findViewById(R.id.spanishCheckBox);
    addressEditText =(EditText) findViewById(R.id.addressEditText);

    sexRadioGroup.setOnCheckedChangeListener(new
                                                 RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group,
                                   int checkedId) {
        switch (checkedId){
          case R.id.menRadioButton:
            Log.d("Sexo","Masculino");
            sex='M';
            break;
          case R.id.womenRadioButton:
            Log.d("Sexo","Femenino");
            sex='F';
            break;
          default:

        }
      }
    });

    spanishCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          Log.d("Interes:", "Español");
          intereses.add("Español");
        }else {
          Log.d("Interes", "no le gusta español");
          if(intereses.contains("Español"))
            intereses.remove("Español");
        }
      }
    });

    statesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String[] states = getResources().getStringArray(R.array.statesValues);
        state=states[position];
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {
        state="Aguascalientes";
      }
    });


    birthdayImageButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DatePickerFragment datePickerFragment=DatePickerFragment.newInstance();
        datePickerFragment.setOnDateListener(MainActivity.this);
        datePickerFragment.show(getSupportFragmentManager(),"Birthday-tag");
      }
    });

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.registerButton);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

      }
    });
  }

  private void hideKeyboard() {
    View view = getCurrentFocus();
    if (view != null) {
      ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
          hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }
  }

  private String getName(){
    String name=nameTextInputLayout.getEditText().getText().toString();
    if(name==null || name.isEmpty() || name.equals("")){
      nameTextInputLayout.setErrorEnabled(true);
      nameTextInputLayout.setError("El nombre no es válido");
    }else{
      nameTextInputLayout.setErrorEnabled(false);
    }

    return name;
  }

  private String getAddress(){
    String address=addressEditText.getText().toString();
    return address;
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onDateSelected(int year, int month, int day) {

  }
}
