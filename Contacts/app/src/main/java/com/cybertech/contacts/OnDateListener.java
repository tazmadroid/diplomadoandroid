package com.cybertech.contacts;

/**
 * Created by Tazmadroid on 25/01/18.
 */

public interface OnDateListener {

  public void onDateSelected(int year, int month, int day);
}
