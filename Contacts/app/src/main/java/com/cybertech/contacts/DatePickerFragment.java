package com.cybertech.contacts;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Tazmadroid on 25/01/18.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

  private OnDateListener onDateListener=null;

  public static DatePickerFragment newInstance(){
    DatePickerFragment datePickerFragment=new DatePickerFragment();
    return datePickerFragment;
  }

  public void setOnDateListener(OnDateListener onDateListener) {
    this.onDateListener = onDateListener;
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    // Use the current date as the default date in the picker
    final Calendar c = Calendar.getInstance();
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    int day = c.get(Calendar.DAY_OF_MONTH);

    // Create a new instance of DatePickerDialog and return it
    return new DatePickerDialog(getActivity(), this, year, month, day);
  }

  @Override
  public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
    if(onDateListener!=null)
      onDateListener.onDateSelected(year,month,dayOfMonth);
  }
}
