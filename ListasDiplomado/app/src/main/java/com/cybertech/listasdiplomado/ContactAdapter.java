package com.cybertech.listasdiplomado;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tazmadroid on 27/01/18.
 */

public class ContactAdapter extends ArrayAdapter<Contact> {

  private Context context=null;
  private List<Contact> contacts=null;

  public ContactAdapter(@NonNull Context context,
                        @NonNull List<Contact> objects) {
    super(context, R.layout.item_contact, objects);
    this.contacts=objects;
    this.context=context;
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    View item=convertView;
    ContactViewHolder contactViewHolder;
    if(item==null){
      LayoutInflater inflater=(LayoutInflater) context.
          getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      item=inflater.inflate(R.layout.item_contact,null);
      contactViewHolder= new ContactViewHolder();
      contactViewHolder.contactImageView=(ImageView) item
          .findViewById(R.id.contactImageView);
      contactViewHolder.nameTextView=(TextView) item
          .findViewById(R.id.nameTextView);
      contactViewHolder.schoolTextView=(TextView) item
          .findViewById(R.id.schoolTextView);
      item.setTag(contactViewHolder);
    }else{
      contactViewHolder=(ContactViewHolder) item.getTag();
    }

    contactViewHolder.contactImageView.setImageResource(R.mipmap.ic_launcher);
    contactViewHolder.nameTextView.setText(contacts.get(position).getName());
    contactViewHolder.schoolTextView.setText(contacts.get(position).getSchool());
    return item;
  }

  @Override
  public int getCount() {
    return contacts.size();
  }

  public Contact getContactSelected(int position){
    return contacts.get(position);
  }

  static class ContactViewHolder{
    ImageView contactImageView;
    TextView nameTextView;
    TextView schoolTextView;
  }
}
