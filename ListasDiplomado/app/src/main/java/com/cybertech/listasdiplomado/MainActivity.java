package com.cybertech.listasdiplomado;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private ListView contactsListView=null;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    contactsListView=(ListView) findViewById(R.id.contactsListView);
    ContactAdapter contactsAdapter = new ContactAdapter(
        getBaseContext(),
        getContacts());
    contactsListView.setAdapter(contactsAdapter);

    contactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getBaseContext(),
            ((ContactAdapter)parent.getAdapter()).getContactSelected(position).getName(),
            Toast.LENGTH_LONG).show();
      }
    });
  }

  public List<Contact> getContacts(){
    List<Contact> contacts = new ArrayList<>();
    contacts.add(new Contact(1,"Juan Solis 1","Benemerito"));
    contacts.add(new Contact(2,"Juan Solis 2","Benemerito"));
    contacts.add(new Contact(3,"Juan Solis 3","Benemerito"));
    contacts.add(new Contact(4,"Juan Solis 4","Benemerito"));
    contacts.add(new Contact(5,"Juan Solis 5","Benemerito"));
    contacts.add(new Contact(6,"Juan Solis 6","Benemerito"));
    contacts.add(new Contact(7,"Juan Solis 7","Benemerito"));
    contacts.add(new Contact(8,"Juan Solis 8","Benemerito"));
    contacts.add(new Contact(9,"Juan Solis 9","Benemerito"));
    contacts.add(new Contact(10,"Juan Solis 10","Benemerito"));
    contacts.add(new Contact(11,"Juan Solis 11","Benemerito"));
    contacts.add(new Contact(12,"Juan Solis 12","Benemerito"));
    return contacts;
  }
}
