package com.cybertech.listviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private ListView peopleListView = null;
  private List<PeopleModel> people= null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    initializePeople();
    peopleListView = (ListView) findViewById(R.id.peopleListView);
    PeopleAdapter peopleAdapter = new PeopleAdapter(this,
        people);
    peopleListView.setAdapter(peopleAdapter);
    peopleListView.setOnItemClickListener(peopleOnItemClickListener);
  }

  public void initializePeople(){
    people=new ArrayList<PeopleModel>();
    for (int indice=0;indice<=10;indice++){
      people.add(new PeopleModel(indice,"Nombre "+indice,"Apellido "+indice,"26/11/1985"));
    }
  }

  private AdapterView.OnItemClickListener peopleOnItemClickListener = new
      AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
          Toast.makeText(getBaseContext(),people.get(i).getName(),Toast.LENGTH_LONG).show();
        }
      };
}
