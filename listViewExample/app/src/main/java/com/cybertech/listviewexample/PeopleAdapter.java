package com.cybertech.listviewexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Tazmadroid on 03/11/17.
 */



public class PeopleAdapter extends ArrayAdapter<PeopleModel> {

  private Context context=null;
  private List<PeopleModel> peopleModelList;

  public PeopleAdapter(@NonNull Context context, @NonNull List<PeopleModel> objects) {
    super(context, R.layout.item_people, objects);
    this.context=context;
    this.peopleModelList=objects;
  }

  @NonNull
  @Override
  public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
    View item = convertView;
    PeopleViewHolder peopleViewHolder;

    if(item == null)
    {
      LayoutInflater inflater = (LayoutInflater) context.
          getSystemService(context.LAYOUT_INFLATER_SERVICE);
      item = inflater.inflate(R.layout.item_people, null);
      peopleViewHolder = new PeopleViewHolder();
      peopleViewHolder.nameTextView=(TextView)item.
          findViewById(R.id.nameTextView);
      peopleViewHolder.lastnameTextView=(TextView)item.
          findViewById(R.id.lastnameTextView);
      peopleViewHolder.birthdayTextView=(TextView)item.
          findViewById(R.id.birthdayTextView);
      item.setTag(peopleViewHolder);
    }else{
      peopleViewHolder=(PeopleViewHolder) item.getTag();
    }


    peopleViewHolder.nameTextView.setText(peopleModelList.get(position).getName());
    peopleViewHolder.lastnameTextView.setText(peopleModelList.get(position).getLastname());
    peopleViewHolder.birthdayTextView.setText(peopleModelList.get(position).getBirthday());

    return(item);
  }

  @Override
  public int getCount() {
    if(peopleModelList!=null){
      return peopleModelList.size();
    }else{
      return 0;
    }
  }

  static class PeopleViewHolder{
    TextView nameTextView;
    TextView lastnameTextView;
    TextView birthdayTextView;
  }
}
