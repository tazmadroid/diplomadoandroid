package com.cybertech.basedatosexample.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cybertech.basedatosexample.model.Contact;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by jaivetorrespineda on 05/01/18.
 */

public class ContactsDatabaseController {

	private Context context=null;
	private ContactsDatabase contactsDatabase=null;
	private SQLiteDatabase sqLiteDatabase=null;


	public ContactsDatabaseController() {
	}

	public ContactsDatabaseController(Context context) {
		this.context = context;
		contactsDatabase=new ContactsDatabase(context);
	}

	public ContactsDatabaseController(Context context, ACCESS_MODE accessMode) {
		this.context = context;
		contactsDatabase=new ContactsDatabase(context);
		if(accessMode==ACCESS_MODE.READ){
			sqLiteDatabase=contactsDatabase.getReadableDatabase();
		}else{
			sqLiteDatabase=contactsDatabase.getWritableDatabase();
		}
	}

	public boolean insertContact (Contact contact) {
		boolean insert = false;
		ContentValues insertValues = new ContentValues();
		insertValues.put("us_id", contact.getId());
		insertValues.put("us_name", contact.getName());
		insertValues.put("us_lastname", contact.getLastname());
		insertValues.put("us_email", contact.getEmail());
		insertValues.put("us_number_phone",contact.getNumberPhone());
		long inserted = sqLiteDatabase.insert("user",
				null, insertValues);
		if (inserted > 0)
			insert = true;

		return insert;
	}

	public boolean deleteContact (int id) {
		String[] args = new String[]{Integer.toString(id)};
		long deleted = sqLiteDatabase.delete("user",
				"us_id=?", args);
		if (deleted > 0)
			return true;
		else
			return false;
	}


	public boolean updateContact(Contact contact){
		boolean updated=false;
		String[] args = new String[]{Integer.toString(contact.getId())};
		ContentValues updatedValues = new ContentValues();
		updatedValues.put("us_id", contact.getId());
		updatedValues.put("us_name", contact.getName());
		updatedValues.put("us_lastname", contact.getLastname());
		updatedValues.put("us_email", contact.getEmail());
		updatedValues.put("us_number_phone",contact.getNumberPhone());
		long update=sqLiteDatabase.update("user",updatedValues,"us_id=?",args);
		if(update>0)
			updated=true;

		return updated;
	}

	public List<Contact> getContacts(){
		List<Contact> contacts=null;
		String[] fields=new String[]{
				"us_id",
				"us_name",
				"us_lastname",
				"us_email",
				"us_number_phone"
		};
		Cursor cursor = sqLiteDatabase.query("user",fields,null,null,
				null,null,null);
		if(cursor.moveToFirst()){
			contacts=new LinkedList<>();
			do{
				int id=cursor.getInt(cursor.getColumnIndex("us_id"));
				String name=cursor.getString(cursor.getColumnIndex("us_name"));
				String lastname=cursor.getString(cursor.getColumnIndex("us_lastname"));
				String email=cursor.getString(cursor.getColumnIndex("us_email"));
				int numberPhone=cursor.getInt(cursor.getColumnIndex("us_number_phone"));
				contacts.add(new Contact(id,name,lastname,numberPhone,email));
			}while (cursor.moveToNext());
		}
		cursor.close();
		return contacts;
	}

	public Contact getContact(int idContact){
		Contact contact=null;
		String[] args = new String[]{Integer.toString(idContact)};
		String[] fields=new String[]{
				"us_id",
				"us_name",
				"us_lastname",
				"us_email",
				"us_number_phone"
		};
		Cursor cursor = sqLiteDatabase.query("user",fields,"us_id=?",args,
				null,null,null);
		if(cursor.moveToFirst()){
			do{
				int id=cursor.getInt(cursor.getColumnIndex("us_id"));
				String name=cursor.getString(cursor.getColumnIndex("us_name"));
				String lastname=cursor.getString(cursor.getColumnIndex("us_lastname"));
				String email=cursor.getString(cursor.getColumnIndex("us_email"));
				int numberPhone=cursor.getInt(cursor.getColumnIndex("us_number_phone"));
				contact=new Contact(id,name,lastname,numberPhone,email);
			}while (cursor.moveToNext());
		}
		cursor.close();
		return contact;
	}
}
