package com.cybertech.basedatosexample.database;

/**
 * Created by jaivetorrespineda on 05/01/18.
 */

public enum ACCESS_MODE {
	READ,
	WRITE;
}
