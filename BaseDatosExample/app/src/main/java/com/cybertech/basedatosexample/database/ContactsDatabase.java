package com.cybertech.basedatosexample.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by jaivetorrespineda on 04/01/18.
 */

public class ContactsDatabase extends SQLiteOpenHelper {

	private static final String CREATE_USER_TABLE="CREATE TABLE `user` (" +
			" `us_id` INT NOT NULL," +
			" `us_name` VARCHAR(50) NULL," +
			" `us_lastname` VARCHAR(100) NULL," +
			" `us_number_phone` INT(12) NULL," +
			" `us_email` VARCHAR(150) NULL," +
			" PRIMARY KEY (`us_id`))";

	private static final String DATABASE_NAME="users";
	private static final int DATABASE_VERSION=1;

	public ContactsDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		try{
			sqLiteDatabase.execSQL(CREATE_USER_TABLE);
		}catch (SQLException e){
			Log.e("[ERROR]", e.getMessage());
		}catch (Exception f){
			Log.e("[ERROR]", f.getMessage());
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
		try{
			sqLiteDatabase.execSQL(CREATE_USER_TABLE);
		}catch (SQLException e){
			Log.e("[ERROR]", e.getMessage());
		}catch (Exception f){
			Log.e("[ERROR]", f.getMessage());
		}
	}
}
