package com.cybertech.basedatosexample.model;

/**
 * Created by jaivetorrespineda on 05/01/18.
 */

public class Contact {

	private int id=0;
	private String name=null;
	private String lastname=null;
	private int numberPhone=0;
	private String email=null;

	public Contact() {
	}

	public Contact(int id, String name, String lastname, int numberPhone, String email) {
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.numberPhone = numberPhone;
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getNumberPhone() {
		return numberPhone;
	}

	public void setNumberPhone(int numberPhone) {
		this.numberPhone = numberPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
