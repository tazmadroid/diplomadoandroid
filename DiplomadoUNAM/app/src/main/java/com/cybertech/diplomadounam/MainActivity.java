package com.cybertech.diplomadounam;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

  public static final int UPDATE_CONTACT=12478;

  @Override
  protected void onStart() {
    super.onStart();
    Log.d("[CICLO]","Start");
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Log.d("[CICLO]","Create");
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent detailIntent=new Intent(getBaseContext(),
            DetailActivity.class);
        detailIntent.putExtra("name_key","Juan Perez");
        Bundle detailBundle= new Bundle();
        detailBundle.putString("name_key","Juan Solis");
        detailIntent.putExtra("name_bundle",detailBundle);
        startActivityForResult(detailIntent,UPDATE_CONTACT);
      }
    });
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if(resultCode==UPDATE_CONTACT){
      Toast.makeText(getBaseContext(),"Se actualizo",Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  protected void onRestart() {
    super.onRestart();
    Log.d("[CICLO]","Restart");
  }

  @Override
  protected void onPause() {
    super.onPause();
    Log.d("[CICLO]","Pause");
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d("[CICLO]","Resume");
  }

  @Override
  protected void onStop() {
    super.onStop();
    Log.d("[CICLO]","Stop");
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.d("[CICLO]","Destroy");
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
