package com.cybertech.fragmentsexample;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class ListFragment extends Fragment  implements OnContactItemClickListener{

  private List<PeopleModel> people= null;
  private RecyclerView listContactRecyclerView=null;

  private boolean isTablet=false;

  public ListFragment() {
    // Required empty public constructor
  }

  public static ListFragment newInstance() {
    ListFragment fragment = new ListFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_list, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    isTablet=getResources().getBoolean(R.bool.is_tablet);
    initializePeople();
    listContactRecyclerView = (RecyclerView) view
        .findViewById(R.id.listContactRecyclerView);
    LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(),
        LinearLayoutManager.VERTICAL,false);
    listContactRecyclerView.setHasFixedSize(true);
    listContactRecyclerView.setLayoutManager(layoutManager);
    PeopleAdapter peopleAdapter = new PeopleAdapter(people);
    peopleAdapter.setOnContactItemClickListener(this);
    listContactRecyclerView.setAdapter(peopleAdapter);
  }

  public void initializePeople(){
    people=new ArrayList<PeopleModel>();
    for (int indice=0;indice<=10;indice++){
      people.add(new PeopleModel(indice,"Nombre "+indice,"Apellido "+indice,"26/11/1985"));
    }
  }

  @Override
  public void onContactClick(PeopleModel peopleModel) {
    if(isTablet){
      getActivity().getSupportFragmentManager().beginTransaction()
          .replace(R.id.detail_container,DetailFragment
              .newInstance(peopleModel),"Detail-container").commit();
    }else{
      Intent detailIntent = new Intent(getActivity(),DetailActivity.class);
      detailIntent.putExtra(DetailFragment.ARG_PEOPLE,peopleModel);
      startActivity(detailIntent);
    }
  }
}
