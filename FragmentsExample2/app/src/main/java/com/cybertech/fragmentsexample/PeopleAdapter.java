package com.cybertech.fragmentsexample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Tazmadroid on 20/11/17.
 */

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.PeopleViewHolder>{

  private List<PeopleModel> peopleModels=null;
  private boolean isTablet=false;

  private OnContactItemClickListener onContactItemClickListener=null;

  public PeopleAdapter(List<PeopleModel> peopleModels) {
    this.peopleModels = peopleModels;
  }

  @Override
  public PeopleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.
            item_people, parent,false);
    return new PeopleViewHolder(item);
  }

  @Override
  public void onBindViewHolder(PeopleViewHolder holder, int position) {
    holder.nameTextView.setText(peopleModels.get(position).getName());
    holder.lastnameTextView.setText(peopleModels.get(position).getLastname());
    holder.birthdayTextView.setText(peopleModels.get(position).getBirthday());
    holder.setPeople(peopleModels.get(position));
    holder.setOnContactItemClickListener(onContactItemClickListener);
  }

  @Override
  public int getItemCount() {
    if(peopleModels!=null){
      return peopleModels.size();
    }else{
      return 0;
    }
  }

  public void setOnContactItemClickListener(OnContactItemClickListener onContactItemClickListener) {
    this.onContactItemClickListener = onContactItemClickListener;
  }

  static class PeopleViewHolder extends RecyclerView.ViewHolder{

    protected TextView nameTextView;
    protected TextView lastnameTextView;
    protected TextView birthdayTextView;

    private OnContactItemClickListener onContactItemClickListener=null;

    private PeopleModel people=null;

    public PeopleViewHolder(final View itemView) {
      super(itemView);
      nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
      lastnameTextView = (TextView) itemView.findViewById(R.id.lastnameTextView);
      birthdayTextView = (TextView) itemView.findViewById(R.id.birthdayTextView);


        itemView.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(onContactItemClickListener!=null){
              onContactItemClickListener.onContactClick(people);
            }
          }
        });

    }

    public void setPeople(PeopleModel people) {
      this.people = people;
    }

    public void setOnContactItemClickListener(OnContactItemClickListener onContactItemClickListener) {
      this.onContactItemClickListener = onContactItemClickListener;
    }
  }
}
