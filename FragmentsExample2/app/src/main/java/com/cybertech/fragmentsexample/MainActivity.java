package com.cybertech.fragmentsexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

  private boolean isTablet=false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    isTablet=getResources().getBoolean(R.bool.is_tablet);
    getSupportFragmentManager().beginTransaction().replace(R.id.main_container,
        ListFragment.newInstance(),"Main").commit();

  }
}
