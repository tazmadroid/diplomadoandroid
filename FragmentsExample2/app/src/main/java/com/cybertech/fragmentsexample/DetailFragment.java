package com.cybertech.fragmentsexample;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DetailFragment extends Fragment {

  public static final String ARG_PEOPLE = "people";

  private PeopleModel people;

  private TextView nameContactTextView=null;
  private TextView birthdayContactTextView=null;

  public DetailFragment() {
    // Required empty public constructor
  }

  public static DetailFragment newInstance(PeopleModel people) {
    DetailFragment fragment = new DetailFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_PEOPLE,people);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      people =(PeopleModel) getArguments().getSerializable(ARG_PEOPLE);
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_detail, container,
        false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    nameContactTextView = view.findViewById(R.id.nameContactTextView);
    birthdayContactTextView = view.findViewById(R.id.birthdayContactTextView);
    if(people!=null){
      nameContactTextView.setText(people.getName()+" "+people.getLastname());
      birthdayContactTextView.setText(people.getBirthday());
    }else{
      nameContactTextView.setText("");
      birthdayContactTextView.setText("");
    }
  }
}
