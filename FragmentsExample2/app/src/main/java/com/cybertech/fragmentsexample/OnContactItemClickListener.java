package com.cybertech.fragmentsexample;

/**
 * Created by Tazmadroid on 27/12/17.
 */

public interface OnContactItemClickListener {

  public void onContactClick(PeopleModel peopleModel);

}
