package com.cybertech.fragmentsexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    PeopleModel peopleModel = (PeopleModel) getIntent().getSerializableExtra(DetailFragment
        .ARG_PEOPLE);
    getSupportFragmentManager().beginTransaction().replace(R
        .id.detail_container,DetailFragment.newInstance(peopleModel),
        "Detail-container").commit();
  }
}
