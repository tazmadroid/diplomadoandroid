package com.cybertech.recyclerviewexample;

/**
 * Created by Tazmadroid on 03/11/17.
 */

public class PeopleModel {

  private int id=0;
  private String name=null;
  private String lastname=null;
  private String birthday=null;

  public PeopleModel() {
  }

  public PeopleModel(int id, String name, String lastname, String birthday) {
    this.id = id;
    this.name = name;
    this.lastname = lastname;
    this.birthday = birthday;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }
}
