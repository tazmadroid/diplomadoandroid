package com.cybertech.recyclerviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private List<PeopleModel> people= null;
  private RecyclerView contactRecyclerView=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    initializePeople();
    contactRecyclerView = (RecyclerView) findViewById(R.id.contactsRecyclerView);
    LinearLayoutManager layoutManager= new LinearLayoutManager(this,
        LinearLayoutManager.VERTICAL,false);
    contactRecyclerView.setHasFixedSize(true);
    contactRecyclerView.setLayoutManager(layoutManager);
    PeopleAdapter peopleAdapter = new PeopleAdapter(people);
    contactRecyclerView.setAdapter(peopleAdapter);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.registerContact:
        Toast.makeText(getBaseContext(),"Selecciono registrar",
            Toast.LENGTH_LONG).show();
        return true;
      case R.id.deleteContact:
        Toast.makeText(getBaseContext(),"Selecciono eliminar",
            Toast.LENGTH_LONG).show();
        return true;
      case R.id.searchContact:
        Toast.makeText(getBaseContext(),"Selecciono buscar",
            Toast.LENGTH_LONG).show();
        return true;
      case R.id.favoritesContact:
        Toast.makeText(getBaseContext(),"Selecciono favoritos",
            Toast.LENGTH_LONG).show();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void initializePeople(){
    people=new ArrayList<PeopleModel>();
    for (int indice=0;indice<=10;indice++){
      people.add(new PeopleModel(indice,"Nombre "+indice,"Apellido "+indice,"26/11/1985"));
    }
  }
}
