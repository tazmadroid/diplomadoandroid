package com.curso.tazmadroid.fragmentsexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

  private Button removeButton=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    removeButton = (Button) findViewById(R.id.removeButton);

    removeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        MainFragment mainFragment = (MainFragment)getSupportFragmentManager().findFragmentById(R.id.mainFragment);
        getSupportFragmentManager().beginTransaction().remove(mainFragment).commit();
      }
    });
  }
}
