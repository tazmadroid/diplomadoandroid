package com.cybertech.recyclercontact;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private RecyclerView contactRecyclerView=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    contactRecyclerView =(RecyclerView) findViewById(R.id.contactRecyclerView);
    //LinearLayoutManager manager=new LinearLayoutManager(this);
    //GridLayoutManager manager=new GridLayoutManager(this,2);
    StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
    contactRecyclerView.setHasFixedSize(true);
    contactRecyclerView.setLayoutManager(manager);
    ContactAdapter contactAdapter = new ContactAdapter(getContacts());
    contactRecyclerView.setAdapter(contactAdapter);
    contactAdapter.setOnContactClickListener(new OnContactClickListener() {
      @Override
      public void onContactClick(Contact contact) {
        Toast.makeText(getBaseContext(),contact.getName(),Toast.LENGTH_SHORT).show();
      }
    });


  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main_menu,menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.registerContact:
        Toast.makeText(getBaseContext(),"Registro",Toast.LENGTH_SHORT).show();
        return true;
      case R.id.deleteContact:
        return true;
      case R.id.searchContact:
        return true;
      case R.id.favoritesContact:
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public List<Contact> getContacts(){
    List<Contact> contacts = new ArrayList<>();
    contacts.add(new Contact(1,"Juan Solis 1","Benemerito"));
    contacts.add(new Contact(2,"Juan Solis 2","Benemerito"));
    contacts.add(new Contact(3,"Juan Solis 3","Benemerito"));
    contacts.add(new Contact(4,"Juan Solis 4","Benemerito"));
    contacts.add(new Contact(5,"Juan Solis 5","Benemerito"));
    contacts.add(new Contact(6,"Juan Solis 6","Benemerito"));
    contacts.add(new Contact(7,"Juan Solis 7","Benemerito"));
    contacts.add(new Contact(8,"Juan Solis 8","Benemerito"));
    contacts.add(new Contact(9,"Juan Solis 9","Benemerito"));
    contacts.add(new Contact(10,"Juan Solis 10","Benemerito"));
    contacts.add(new Contact(11,"Juan Solis 11","Benemerito"));
    contacts.add(new Contact(12,"Juan Solis 12","Benemerito"));
    return contacts;
  }
}
