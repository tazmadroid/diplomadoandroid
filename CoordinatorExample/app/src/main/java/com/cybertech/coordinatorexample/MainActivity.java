package com.cybertech.coordinatorexample;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

  private EditText nameEditText=null;
  private EditText ageEditText=null;
  private RadioGroup sexRadioGroup=null;
  private CheckBox spanishCheckBox=null;
  private CheckBox historyCheckBox=null;
  private CheckBox mathematicsCheckBox=null;
  private CheckBox englishCheckBox=null;
  private CheckBox ecologyCheckBox=null;
  private CheckBox politicsCheckBox=null;
  private FloatingActionButton registerButton=null;
  private TextView resultTextView=null;

  private String name=null;
  private int age=0;
  private char sex=' ';
  private ArrayList<String> topics=null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    nameEditText = (EditText) findViewById(R.id.nameEditText);
    ageEditText = (EditText) findViewById(R.id.ageEditText);
    sexRadioGroup = (RadioGroup) findViewById(R.id.sexRadioGroup);
    spanishCheckBox = (CheckBox) findViewById(R.id.spanishCheckBox);
    historyCheckBox = (CheckBox) findViewById(R.id.historyCheckBox);
    mathematicsCheckBox = (CheckBox) findViewById(R.id.mathematicsCheckBox);
    englishCheckBox = (CheckBox) findViewById(R.id.englishCheckBox);
    ecologyCheckBox = (CheckBox) findViewById(R.id.ecologyCheckBox);
    politicsCheckBox = (CheckBox) findViewById(R.id.politicCheckBox);
    resultTextView = (TextView) findViewById(R.id.resultTextView);

    registerButton = (FloatingActionButton) findViewById(R.id.registerButton);
    registerButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        name=nameEditText.getText().toString();
        age=Integer.parseInt(ageEditText.getText().toString());


        resultTextView.setText(name+','+age+","+sex+","+topics.toString());
      }
    });

    nameEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        String inputString = String.valueOf(s.subSequence(start, start + count));
        Log.v("Prueba",inputString);
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });

    sexRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId){
          case R.id.menRadioButton:
            Log.d("Sexo seleccionado","Masculino");
            sex='M';
            break;
          case R.id.womenRadioButton:
            Log.d("Sexo seleccionado","Femenino");
            sex='F';
            break;
          default:
            Log.d("Sexo seleccionado","Masculino");
            break;
        }
      }
    });

    spanishCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          topics.add("Español");
          Log.d("Materia seleccionada", "Español");
        }else{
          if(topics.contains("Español")){
            topics.remove("Español");
          }
        }
      }
    });

    mathematicsCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
          topics.add("Matematicas");
        }else{
          if(topics.contains("Matematicas")){
            topics.remove("Matematicas");
          }
        }
      }
    });

    historyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
          topics.add("Historia");
        }else{
          if(topics.contains("Historia")){
            topics.remove("Historia");
          }
        }
      }
    });

    englishCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
          topics.add("Ingles");
        }else{
          if(topics.contains("Ingles")){
            topics.remove("Ingles");
          }
        }
      }
    });

    ecologyCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
          topics.add("Ecologia");
        }else{
          if(topics.contains("Ecologia")){
            topics.remove("Ecologia");
          }
        }
      }
    });

    politicsCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
          topics.add("Politica");
        }else{
          if(topics.contains("Politica")){
            topics.remove("Politica");
          }
        }
      }
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
