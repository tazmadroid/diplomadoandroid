package com.cybertech.fragmentcontact.database;

/**
 * Created by Tazmadroid on 03/02/18.
 */

public enum ACCESS_MODE {
  READ,
  WRITE;
}
