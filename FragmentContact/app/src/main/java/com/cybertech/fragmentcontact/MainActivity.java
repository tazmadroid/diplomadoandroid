package com.cybertech.fragmentcontact;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show();
      }
    });

    getSupportFragmentManager().beginTransaction().
        replace(R.id.mainContainer,
            MainFragment.newInstance()).commit();
  }

  @Override
  public void onBackPressed() {
    boolean isTablet=getResources().getBoolean(R.bool.is_tablet);
    if(isTablet) {
      Fragment fragment = getSupportFragmentManager().findFragmentByTag("3");
      getSupportFragmentManager().beginTransaction().hide(fragment).commit();
    } else {
      super.onBackPressed();
    }
  }

}
