package com.cybertech.fragmentcontact;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DetailActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_detail);
    Contact contact=(Contact)(getIntent().getExtras())
        .getSerializable(DetailFragment.KEY_CONTACT);
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.detailContainer,
        DetailFragment.newInstance(contact)).commit();
  }
}
