package com.cybertech.fragmentcontact;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Tazmadroid on 02/02/18.
 */

public class DetailFragment extends Fragment {

  public static final String KEY_CONTACT="contact";

  private Contact contact=null;

  private TextView nameTextView=null;

  public static DetailFragment newInstance(Contact contact){
    DetailFragment detailFragment=new DetailFragment();
    Bundle args=new Bundle();
    args.putSerializable(KEY_CONTACT,contact);
    detailFragment.setArguments(args);
    return detailFragment;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if(getArguments()!=null) {
      contact = (Contact) getArguments().
          getSerializable(KEY_CONTACT);
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    return inflater.inflate(R.layout.detail_fragment,container,false);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    nameTextView=(TextView) view.findViewById(R.id.textView);
    if(contact!=null)
      nameTextView.setText(contact.getName());
  }
}
