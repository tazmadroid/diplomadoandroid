package com.cybertech.fragmentcontact.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Tazmadroid on 03/02/18.
 */

public class DataBase extends SQLiteOpenHelper {

  private static final String CREATE_USER_TABLE="CREATE TABLE" +
      " 'user'("+
      "'us_id' INT NOT NULL, "+
      "'us_name' VARCHAR(50) NULL, "+
      "'us_lastname' VARCHAR(100) NULL, "+
      "'us_school' VARCHAR(100) NULL, "+
      "'us_age' INT, PRIMARY KEY('us_id'))";

  private static final String DATABASE_NAME="users";

  private static final int VERSION=1;

  public DataBase(Context context) {
    super(context, DATABASE_NAME, null, VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    try {
      db.execSQL(CREATE_USER_TABLE);
    }catch (SQLException s){
      Log.e("[DBERROR]",s.getMessage());
    }catch (Exception f){
      Log.e("[DBERROR]",f.getMessage());
    }
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    try {
      db.execSQL(CREATE_USER_TABLE);
    }catch (SQLException s){
      Log.e("[DBERROR]",s.getMessage());
    }catch (Exception f){
      Log.e("[DBERROR]",f.getMessage());
    }
  }
}
