package com.cybertech.fragmentcontact;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cybertech.fragmentcontact.database.ACCESS_MODE;
import com.cybertech.fragmentcontact.database.DataBase;
import com.cybertech.fragmentcontact.database.DatabaseController;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment
    implements OnContactClickListener{

  private boolean isTablet=false;
  private RecyclerView contactsRecyclerView=null;
  private DatabaseController databaseController=null;
  private SharedPreferences dataPreferences=null;

  public MainFragment() {
    // Required empty public constructor
  }

  // TODO: Rename and change types and number of parameters
  public static MainFragment newInstance() {
    MainFragment fragment = new MainFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    dataPreferences=getActivity()
        .getSharedPreferences("usersSettings",
        Context.MODE_PRIVATE);

    SharedPreferences.Editor editor=dataPreferences.edit();
    editor.putString("us_key","Isabel Catolica");
    editor.commit();

    Toast.makeText(getContext(),dataPreferences
        .getString("us_key","hola"),
        Toast.LENGTH_LONG).show();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_main, container, false);
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    isTablet=getResources().getBoolean(R.bool.is_tablet);
    contactsRecyclerView = (RecyclerView) view.findViewById(R
        .id.contactsRecyclerView);
    if(isTablet){
      GridLayoutManager manager = new GridLayoutManager(getActivity(),
          2);
      contactsRecyclerView.setHasFixedSize(true);
      contactsRecyclerView.setLayoutManager(manager);
    }else{
      LinearLayoutManager manager = new LinearLayoutManager(getActivity(),
          LinearLayoutManager.VERTICAL,false);
      contactsRecyclerView.setHasFixedSize(true);
      contactsRecyclerView.setLayoutManager(manager);
    }

    ContactAdapter contactAdapter=new ContactAdapter(getContacts());
    contactAdapter.setOnContactClickListener(this);
    contactsRecyclerView.setAdapter(contactAdapter);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.main_menu,menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()){
      case R.id.registerContact:
        insertContact();
        return true;
      case R.id.deleteContact:

        return true;
      case R.id.searchContact:
        return true;
      case R.id.favoritesContact:
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void insertContact(){
    Contact contact = new Contact(13,"Isabel Vargas",
        "Nicolas Bravo","S/D",34);
    databaseController=new DatabaseController(getContext(),
        ACCESS_MODE.WRITE);
    if(databaseController.insertContact(contact)) {
      Toast.makeText(getContext(), "Se registro",
          Toast.LENGTH_SHORT).show();
    }else{
      Toast.makeText(getContext(), "No se registro",
          Toast.LENGTH_SHORT).show();
    }
    databaseController.close();
    databaseController=null;
  }

  public List<Contact> getContacts(){
    List<Contact> contacts = new ArrayList<>();
    contacts.add(new Contact(1,"Juan Solis 1","Benemerito"));
    contacts.add(new Contact(2,"Juan Solis 2","Benemerito"));
    contacts.add(new Contact(3,"Juan Solis 3","Benemerito"));
    contacts.add(new Contact(4,"Juan Solis 4","Benemerito"));
    contacts.add(new Contact(5,"Juan Solis 5","Benemerito"));
    contacts.add(new Contact(6,"Juan Solis 6","Benemerito"));
    contacts.add(new Contact(7,"Juan Solis 7","Benemerito"));
    contacts.add(new Contact(8,"Juan Solis 8","Benemerito"));
    contacts.add(new Contact(9,"Juan Solis 9","Benemerito"));
    contacts.add(new Contact(10,"Juan Solis 10","Benemerito"));
    contacts.add(new Contact(11,"Juan Solis 11","Benemerito"));
    contacts.add(new Contact(12,"Juan Solis 12","Benemerito"));
    return contacts;
  }

  @Override
  public void onContactClick(Contact contact) {
    if(isTablet){
      getActivity().getSupportFragmentManager()
          .beginTransaction().replace(R.id.detailContainer,
          DetailFragment.newInstance(contact),Integer.toString(contact.getId()))
          .commit();
    }else{
      Intent detailIntent = new Intent(getActivity(),
          DetailActivity.class);
      detailIntent.putExtra(DetailFragment.KEY_CONTACT,contact);
      startActivity(detailIntent);
    }
  }
}
