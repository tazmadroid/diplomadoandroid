package com.cybertech.fragmentcontact.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cybertech.fragmentcontact.Contact;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tazmadroid on 03/02/18.
 */

public class DatabaseController {

  private Context context=null;
  private DataBase dataBase=null;
  private SQLiteDatabase sqLiteDatabase=null;

  public DatabaseController() {
  }

  public DatabaseController(Context context, ACCESS_MODE accessMode) {
    this.context = context;
    dataBase=new DataBase(context);
    if(accessMode==ACCESS_MODE.READ){
      sqLiteDatabase=dataBase.getReadableDatabase();
    }else if(accessMode==ACCESS_MODE.WRITE){
      sqLiteDatabase=dataBase.getWritableDatabase();
    }
  }

  public boolean insertContact(Contact contact){
    boolean inserted=false;
    ContentValues insertValues=new ContentValues();
    insertValues.put("us_id",contact.getId());
    insertValues.put("us_name",contact.getName());
    insertValues.put("us_lastname",contact.getAddress());
    insertValues.put("us_school",contact.getSchool());
    insertValues.put("us_age",contact.getAge());
    long insert=sqLiteDatabase.insert("user",
        null,insertValues);
    if(insert>0)
      inserted=true;
    else
      inserted=false;

    return inserted;
  }

  public boolean deleteContact(int id){
    String[] args=new String[]{
      Integer.toString(id)
    };
    long delete=sqLiteDatabase.delete("user",
        "us_id=?",args);
    if(delete>0)
      return true;
    else
      return false;
  }

  public boolean updateContact(Contact contact){
    String[] args=new String[]{
        Integer.toString(contact.getId())
    };
    ContentValues updatedValues=new ContentValues();
    updatedValues.put("us_id",contact.getId());
    updatedValues.put("us_name",contact.getName());
    updatedValues.put("us_lastname",contact.getAddress());
    updatedValues.put("us_school",contact.getSchool());
    updatedValues.put("us_age",contact.getAge());
    long update=sqLiteDatabase.update("user",
        updatedValues,"us_id=?",args);
    if(update>0)
      return true;
    else
      return false;
  }

  public List<Contact> getContacts(){
    List<Contact> contacts=null;
    String[] fields=new String[]{
        "us_id",
        "us_name",
        "us_lastname",
        "us_school",
        "us_age"
    };
    Cursor cursor = sqLiteDatabase.query("user",fields,null,null,null,null,null);
    if(cursor.moveToFirst()){
      contacts=new LinkedList<>();
      do{
        int id=cursor.getInt(cursor
            .getColumnIndex("us_id"));
        String name =cursor.getString(cursor
            .getColumnIndex("us_name"));
        String lastname=cursor.getString(cursor
            .getColumnIndex("us_lastname"));
        String school=cursor.getString(cursor
            .getColumnIndex("us_school"));
        int age=cursor.getInt(cursor
            .getColumnIndex("us_age"));
        contacts.add(new Contact(id,name,school,lastname,age));
      }while (cursor.moveToNext());
    }
    cursor.close();
    return contacts;
  }

  public Contact getContact(int id){
    Contact contact=null;
    String[] args=new String[]{
        Integer.toString(id)
    };
    String[] fields=new String[]{
        "us_id",
        "us_name",
        "us_lastname",
        "us_school",
        "us_age"
    };
    Cursor cursor = sqLiteDatabase.query("user",fields,
        "us_id=?",args,null,null,
        null);
    if(cursor.moveToFirst()){
      contact=new Contact();
        contact.setId(cursor.getInt(cursor
            .getColumnIndex("us_id")));
        contact.setName(cursor.getString(cursor
            .getColumnIndex("us_name")));
        contact.setAddress(cursor.getString(cursor
            .getColumnIndex("us_lastname")));
        contact.setSchool(cursor.getString(cursor
            .getColumnIndex("us_school")));
        contact.setAge(cursor.getInt(cursor
            .getColumnIndex("us_age")));
    }
    cursor.close();
    return contact;
  }

  public void close(){
    sqLiteDatabase.close();
    dataBase.close();
  }

}
